export const environment = {
  production: true,
  API_IP_CAFFENIO: 'http://127.0.0.1:52833/api/',
  API_IP_RO: 'http://127.0.0.1:8000/'
};
