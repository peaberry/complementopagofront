import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable()
export class SessionService {

    constructor (private apiService: ApiService) {}

    private store(content: Object) {
        localStorage.setItem('user', JSON.stringify(content));        
    }

    private retrieve() {
        const storedToken: string = localStorage.getItem('user');
        if (!storedToken) { /** error */ }
        return storedToken;
    }

    /**
     * Funcion para hacer login y generar un token
     * @param username email
     * @param password contraseña
     */
    public async generateNewToken(username: string, password: string) {
        try {
            const token = await this.apiService.POST(
                'get-token/',
                {
                    username: username,
                    password: password,
                },
                'ro',
                false,
                false,
                false,
                'body'
            );
            this.store(token);
            return token;
        } catch (err) {
            throw(err);
        }
    }

    /** Recuperar token */
    public retrieveToken() {
        try {
            const storedToken = JSON.parse(this.retrieve());
            const token = (storedToken) ? storedToken.token : undefined;
            return token;
        } catch (err) {
            console.error(err);
        }
    }

    public borrarToken() {
        localStorage.clear();
    }

}
