import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/toPromise';

import { environment } from './../../environments/environment';
const url_caffenio: string = environment.API_IP_CAFFENIO;
const url_ro: string = environment.API_IP_RO;

@Injectable()
export class ApiService {

    constructor (private http: HttpClient) {}

    /**
     * Función para realizar llamadas GET
     * @param path string. Ruta especifica del endpoint
     * @param api string. Especifica que ip se va usar
     * @param bichi boolean. Para saber si utilizamos el prefijo del api o unicamente el path proporcionado
     * @param awaitToken boolean. Para saber si necesitamos el token en esta llamada
     * @param observe string. response = full | body = body del resp
     */
    async GET(path: string, api: string, bichi: boolean, awaitToken: boolean, observe: string): Promise<any> {
        // Definiendo url base
        const url = this.urlBase(api);
        // Definiendo endpoint
        const endpoint = (bichi) ? path : url + path;
        // Definiendo headers
        const token = this.retrieveToken();
        const headers = new HttpHeaders({
            ...(token ? {'Authorization': `Token ${token}`} : {'Authorization': ''})
        });
        // Configurando las opciones para la llamada
        const options: Object = {
            headers: headers,
            observe: observe
        };
        // Haciendo la llamada
        return this.http.get(endpoint, options).retry(2).toPromise();
    }

    /**
     * Función para realizar llamadas POST
     * @param path string. Ruta especifica del endpoint
     * @param body object. contenido a postear
     * @param api string. Especifica que ip se va usar
     * @param formData boolean. FormData o normalon
     * @param observe string. response = full | body = body del resp
     */
    async POST(path: string, body: Object, api: string, bichi: boolean, formData: boolean, awaitToken: boolean, observe: string): Promise<any> {
        // Definiendo url base
        const url = this.urlBase(api);
        // Definiendo endpoint
        const endpoint = (bichi) ? path : url + path;
        // Definiendo headers
        const token = this.retrieveToken();
        const headers = new HttpHeaders({
            ...(token ? {'Authorization': `Token ${token}`} : {'Authorization': ''}),
            ...(formData ? {} : {'Content-Type': 'application/json'})
        });
        // Configurando opciones para la llamada
        const options: Object = {
            headers: headers,
            observe: observe
        };
        // Haciendo la llamada
        return this.http.post(endpoint, body, options).retry(2).toPromise();
    }

    /**
     * Función para realizar llamadas POST donde quieres ver el progreso de avance
     * @param path string. Ruta especifica del endpoint
     * @param body object. contenido a postear
     * @param formData boolean. FormData o normalon
     * @param observe string. response = full | body = body del resp
     */
    POSTprogress(path: string, body: Object, api: string, bichi: boolean, formData: boolean, awaitToken: boolean, observe: string): HttpRequest<Object> {
        // Definiendo url base
        const url = this.urlBase(api);
        // Definiendo endpoint
        const endpoint = (bichi) ? path : url + path;
        // Definiendo headers
        const token = this.retrieveToken();
        const headers = new HttpHeaders({
            ...(token ? {'Authorization': `Token ${token}`} : {'Authorization': ''}),
            ...(formData ? {} : {'Content-Type': 'application/json'})
        });
        // Configurando opciones para la llamada
        const options: Object = {
            headers: headers,
            observe: observe,
            reportProgress: true
        };
        // Haciendo la llamada
        return new HttpRequest('POST', endpoint, body, options);
        /* return this.http.post(endpoint, body, options); */
    }

    /**
     * Función para realizar llamadas PUT
     * @param path string. Ruta especifica del endpoint
     * @param body object. contenido a postear
     * @param bichi boolean. Prefijo o sin prefijo
     * @param formData boolean. FormData o normalon
     * @param awaitToken boolean. Para saber si necesitamos el idToken en esta llamada y esperarlo o no
     * @param observe string. response = full | body = body del resp
     */
    async PUT(path: string, body: Object, api: string, bichi: boolean, formData: boolean, awaitToken: boolean, observe: string): Promise<any> {
        // Definiendo url base
        const url = this.urlBase(api);
        // Definiendo endpoint
        const endpoint = (bichi) ? path : url + path;
        // Definiendo headers
        const token = this.retrieveToken();
        const headers = new HttpHeaders({
            ...(token ? {'Authorization': `Token ${token}`} : {'Authorization': ''}),
            ...(formData ? {} : {'Content-Type': 'application/json'})
        });
        // Configurando opciones para la llamada
        const options: Object = {
            headers: headers,
            observe: observe
        };
        // Haciendo la llamada
        return this.http.put(endpoint, body, options).retry(2).toPromise();
    }

    /**
     * Función para realizar llamadas DELETE
     * @param path string. Ruta especifica del endpoint
     * @param observe string. response = full | body = body del resp
     */
    async DELETE(path: string, api: string, bichi: boolean, awaitToken: boolean, observe: string) {
        // Definiendo url base
        const url = this.urlBase(api);
        // Definiendo endpoint
        const endpoint = (bichi) ? path : url + path;
        // Definiendo headers
        const token = this.retrieveToken();
        const headers = new HttpHeaders({
            ...(token ? {'Authorization': `Token ${token}`} : {'Authorization': ''}),
        });
        // Configurando las opciones para la llamada
        const options: Object = {
            headers: headers,
            observe: observe
        };
        // Haciendo la llamada
        return this.http.delete(endpoint, options).retry(2).toPromise();
    }

    /** Funcioncita para regresar el url base a utilizar */
    urlBase(api: string) {
        switch (api) {
            case 'ro': return url_ro;
            case 'caffenio': return url_caffenio;
            default: return '';
        }
    }

    /**********************************/
    /** Shit de localstorage */
    /**********************************/

    private store(content: Object) {
        localStorage.setItem('user', JSON.stringify(content));
    }

    private retrieve() {
        const storedToken: string = localStorage.getItem('user');
        if (!storedToken) { /** error */ }
        return storedToken;
    }

    /**
     * Funcion para hacer login y generar un token
     * @param username email
     * @param password contraseña
     */
    public async generateNewToken(username: string, password: string) {
        try {
            const token = await this.POST(
                'get-token/',
                {
                    username: username,
                    password: password,
                },
                'ro',
                false,
                false,
                false,
                'body'
            );
            this.store(token);
            return token;
        } catch (err) {
            throw(err);
        }
    }

    /** Recuperar token */
    public retrieveToken() {
        try {
            const storedToken = JSON.parse(this.retrieve());
            const token = (storedToken) ? storedToken.token : undefined;            
            return token;
        } catch (err) {
            console.error(err);
        }
    }

    public borrarToken() {
        localStorage.clear();
    }

}
