import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';

import { ApiService } from './api.service';

const rutasAdmin: string[] = [
    '/dashboard',
    '/dashboard/complementos', '/dashboard/invitaciones', '/dashboard/notificaciones'
];
const rutasUsuario: string[] = [
    '/dashboard', '/dashboard/complementos'
];

@Injectable()
export class AuthGuardService implements CanActivate {

    user: any;

    constructor(private router: Router, private apiService: ApiService) {}

    async canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {
        const path = this.getPath(route);
        // Validando token
        const token = await this.tokenValido();
        if (token) {
            try {
                let rutas;
                if (this.user.user.is_staff) {
                    rutas = rutasAdmin;
                } else {
                    rutas = rutasUsuario;
                }
                if (rutas.filter(ruta => ruta === path).length > 0) {
                    return true;
                } else {
                    this.router.navigate(['/sinacceso']);
                    return false;
                }
            } catch (err) {
                this.router.navigate(['/sinacceso']);
                return false;
            }
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }

    async canActivateChild(route: ActivatedRouteSnapshot): Promise<boolean> {
        const path = this.getPath(route);
        // Validando token
        const token = await this.tokenValido();
        if (token) {
            try {
                let rutas;
                if (this.user.user.is_staff) {
                    rutas = rutasAdmin;
                } else {
                    rutas = rutasUsuario;
                }
                if (rutas.filter(ruta => ruta === path).length > 0) {
                    return true;
                } else {
                    this.router.navigate(['/sinacceso']);
                    return false;
                }
            } catch (err) {
                this.router.navigate(['/sinacceso']);
                return false;
            }
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }

    /**
     * Funcion para generar el path de la ruta sin parametros o queryparams
     * @param rSnap ActivatedRouteSnapshot de la ruta por activar / activa
     */
    getPath(rSnap: ActivatedRouteSnapshot): string {
        try {
            let path = '';
            for (const url of rSnap.url) {
                if (Object.keys(rSnap.params).length > 0) {
                    for (const param in rSnap.params) {
                        if (rSnap.params[param] !== url.path) {
                            path = path + '/' + url.path;
                        }
                    }
                } else {
                    path = path + '/' + url.path;
                }
            }
            if (rSnap.parent) { path = this.getPath(rSnap.parent) + path; }
            return path;
        } catch (err) { console.error(err); }
    }

    /** Funcion para verificar si el token es valido */
    async tokenValido() {
        try {
            const user = await this.apiService.GET('caffeniousers/current/', 'ro', false, true, 'body');
            this.user = user;
            return true;
        } catch (err) {
            return false;
        }
    }

    /** User info desde el backend */
    public getUser(): any {
        return this.user;
    }

}
