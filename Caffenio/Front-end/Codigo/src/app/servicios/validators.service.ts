import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable()
export class ValidatorsService {

    /**
     * Validator que compara dos formControl y se cumple si ambos tienen el mismo valor
     * @param c1 formControl 1
     * @param c2 formControl 2
     */
    matchEmail(c1: FormControl, c2: FormControl) {
        if (c1 && c2) {
            return (c1.value === c2.value) ? null : {
                matcherror: {
                    valid: false
                }
            };
        }
        return null;
    }

    /**
     * Validator que compara dos formControl que contienen un datestring,
     * y verifica que la proveniente del form control sea mayor a la minima
     * @param cDate formcontrol tipo date
     * @param min string date string
     */
    minDate(cDate: FormControl, min: string) {
        const fecha = new Date(cDate.value);
        const fechaMin = new Date(min);
        return (fecha >= fechaMin) ? null : {
            minFecha: {
                valid: false
            }
        };
    }

}
