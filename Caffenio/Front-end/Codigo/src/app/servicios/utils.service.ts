import { Injectable } from '@angular/core';

@Injectable()
export class UtilsService {

    /**
     * Función bien perra que remueve acentos de un string
     * @param string string con acentos
     */
    remueveAcentos(string: string): string {
        const regex = /[ÁÉÍÓÚáéíóú]/g;
        const translate = {
            'Á': 'A', 'É': 'E', 'Í': 'I', 'Ó': 'O', 'Ú': 'U',
            'á': 'a', 'é': 'e', 'í': 'i', 'ó': 'o', 'ú': 'u'
        };
        try {
            string = string.replace(regex, function(match) { return translate[match]; } );
        } catch (err) {
            console.log(err, string);
            return '';
        }
        return string;
    }

    /**
     * Funcion que remueve los acentos de un string y lo vuelve minusculas
     * @param string string con acentos y mayusculas
     */
    remueveAcentosMinusculas(string: string): string {
        string = this.remueveAcentos(string);
        string = string.toLowerCase();
        return string;
    }

    /**
     * Funcion que remueve etiquetas html de un string y lo devuelve bichito
     * @param string string html
     */
    remueveHTML(string: string): string {
        return string ? String(string).replace(/<[^>]+>/gm, '') : '';
    }

}
