import { RouterModule, Routes } from '@angular/router';

/** Componentes */
import { DashboardComponent } from './dashboard/dashboard.component';
import { NoDashboardComponent } from './noDashboard/noDashboard.component';
import { CuatroCeroTresComponent } from './noDashboard/403/403.component';

/** Child routes */
import { dashboardRoutes } from './dashboard/dashboard.routes';
import { noDashboardRoutes } from './noDashboard/noDashboard.routes';
import { AuthGuardService } from './servicios/auth-guard.service';

const appRoutes: Routes = [
    {
        path: 'dashboard', component: DashboardComponent, children: dashboardRoutes,
        canActivate: [AuthGuardService], canActivateChild: [AuthGuardService]
    },
    { path: 'sinacceso', component: CuatroCeroTresComponent },
    /* { path: 'nodashboard', component: NoDashboardComponent, children: noDashboardRoutes }, */
    { path: '', component: NoDashboardComponent, children: noDashboardRoutes },
    { path: '**', redirectTo: '' }
];

export const Routing = RouterModule.forRoot(appRoutes);
