import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/** Modulos */

// Pipes
import { FormaErrorPipe } from './pipes/formaError.pipe';
import { DataTableComponent } from './datatable/dataTable.component';

/** Directives */

/** Componentes */

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        FormaErrorPipe,
        DataTableComponent
    ],
    exports: [
        FormaErrorPipe,
        DataTableComponent
    ]
})
export class SharedModule { }
