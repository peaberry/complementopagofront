import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'formaError'})
export class FormaErrorPipe implements PipeTransform {
    transform(value: Object, type: String ): String {

        let errorMsg = '';

        if (value['pattern']) {
            switch (type) {
                case 'texto':
                    errorMsg += 'Solo texto (Letras, números, comas y puntos). ';
                    break;
                case 'textosinpuntuacion':
                    errorMsg += 'Solo texto (Letras y números). ';
                    break;
                case 'sololetras':
                    errorMsg += 'Solo texto (Únicamente letras y espacios). ';
                    break;
                case 'numeros':
                    errorMsg += 'Solo números. ';
                    break;
                case 'telefono':
                    errorMsg += 'Teléfono invalido (Ejemplo: +526621102030). ';
                    break;
                case 'email':
                    errorMsg += 'Correo electrónico invalido. ';
                    break;
                case 'rfc':
                    errorMsg += 'RFC invalido. ';
                    break;
                case 'placas':
                    errorMsg += 'Placa invalida. ';
                    break;
                case 'dinero':
                    errorMsg += 'Números, punto decimal y 2 decimales. ';
                    break;
                default:
                    errorMsg += 'Dato invalido. ';
            }
        }

        if (value['required']) {
            errorMsg += 'Campo requerido. ';
        }

        if (value['minlength']) {
            errorMsg += `Minimo ${value['minlength'].requiredLength} caracteres. `;
        }

        if (value['maxlength']) {
            errorMsg += `Máximo ${value['maxlength'].requiredLength} caracteres. `;
        }

        if (value['email']) {
            errorMsg += 'Correo electrónico invalido. ';
        }

        if (value['minFecha']) {
            errorMsg += 'Fecha minima: Hoy';
        }

        if (value['matcherror']) {
            switch (type) {
                case 'password':
                    errorMsg += 'Las contraseñas no coinciden. ';
                    break;
                default:
                    errorMsg += 'Los valores no coinciden. ';
            }
        }

        return errorMsg;
    }
}
