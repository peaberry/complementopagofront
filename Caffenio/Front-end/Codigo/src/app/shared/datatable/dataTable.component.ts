import { Component, Input, Output, OnInit, OnChanges, AfterViewChecked, OnDestroy, SimpleChanges, EventEmitter, Renderer2 } from '@angular/core';
import { SafeHtml, DomSanitizer } from '@angular/platform-browser';
import { UtilsService } from './../../servicios/utils.service';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Component({
    selector: 'caffenio-datatable',
    templateUrl: './dataTable.component.html',
    styleUrls: ['./dataTable.component.css']
})
export class DataTableComponent implements OnInit, OnChanges, AfterViewChecked, OnDestroy {

    Object: Object = Object; // Referencia al prototype Object ?

    /** Entradas */
    @Input() titulo: string;
    @Input() columnas: string[];
    @Input() datos: any[];
    @Input() config: any;
    /* config = {columnDef: {'key': function(): string {return 'value';} } }; */
    @Input() tabs: any[];
    /* tabs = [ { 'tab-id': 'value', 'tab-title': 'value', 'active': false }, {...}, ... ] */

    /** Salidas */
    // Event emitter de los botones
    @Output() btnClickEvent = new EventEmitter();
    eventos: any[] = [];

    /** Datos de la tabla */
    datosTabla: any[];
    // Controles de la tabla y paginación
    controlCantidad = 10;
    paginas: number[];
    paginaActiva: number;
    filtro: string;
    filtroChanged: Subject<string> = new Subject<string>();
    // Variable auxiliar para tabs de la tabla
    tabActivo: string;

    constructor(private utils: UtilsService, private sanitizer: DomSanitizer, private renderer: Renderer2) {}

    /**
     * Lo uso para detectar cambios en los datos que vienen del componente padre y alimentan la tabla
     * @param changes objeto con todo lo que cambio.
     */
    ngOnChanges(changes: SimpleChanges) {
        if (changes.datos) {
            // Preparando datos
            this.datos = changes.datos.currentValue;
            this.configTabla();
            // Acomodar datos a mostrar
            this.generarPaginacion(1);
            // Remover eventos
            this.eventos = this.removerEventos(this.eventos);
        }
    }

    ngOnInit() {
        // Subject/Observable subscription del valor del input filtro para la tabla
        this.filtroChanged.debounceTime(500).distinctUntilChanged().subscribe(value => {
            this.filtro = value;
            // Haz algo con el valor
            this.aplicarFiltro();
        });
    }

    ngAfterViewChecked() {
        // Agregando un event emitter a cada boton de la tabla. Unicamente cuando no existen eventos (se actualizaron los datos)
        if (this.eventos.length < 1) {
            this.addEventEmitterToTag('button');
            this.addEventEmitterToTag('input');
        }
    }

    ngOnDestroy() {
        this.filtroChanged.unsubscribe();
    }

    /**
     * Funcion para cambiar la tab activa de la tabla. Genera un evento falso para actualizar la tabla desde el componente padre.
     * @param event
     */
    onTabChange(event) {
        // consiguiendo valor
        const btn = event.target || event.srcElement || event.currentTarget;
        const id = btn.attributes['id'].nodeValue;
        // añadiendo la clase btn-ActualizarTabla para emular el boton de actualizar con el evento falso
        event.target.classList.add('btn-actualizarTabla');
        // desactivando todos y activando el bueno
        for (const tab of this.tabs) {
            if (tab['tab-id'] !== id) {
                tab['active'] = false;
            } else {
                tab['active'] = true;
            }
        }
        // pasando valor
        this.tabActivo = id;
        // emitiendo evento falso
        this.datosTablaReset();
        this.btnClickEvent.emit({
            'event': event.target,
            'tabActivo': this.tabActivo
        });
    }

    /** Configurar tabla o datos antes de todo con el objeto de config */
    configTabla() {
        try {
            // Cambiando el array de datos para añadir la version modificada segun la config y mostrarla en la tabla
            for (const dato of this.datos) {
                for (const column in dato) {
                    if (true) {
                        // Definicion de datos de las columnas. Solo matchea si el key es hijo directo del objeto
                        if (this.config.columnDef.hasOwnProperty(column)) {
                            const res = this.config.columnDef[column](dato);
                            dato[column] = {
                                value: dato[column],
                                html: this.sanitizer.bypassSecurityTrustHtml(res),
                                htmlraw: this.utils.remueveHTML(res),
                                style: ''
                            };
                        } else {
                            dato[column] = {
                                value: dato[column]
                            };
                        }
                        // Estilos
                        if (this.config.columnStyle.hasOwnProperty(column)) {
                            dato[column]['style'] = this.config.columnStyle[column]();
                        }
                    }
                }
            }
        } catch (err) {
            // console.log('Todavia no llegan los datos Async: ' + err.message);
        }
    }

    /**
     * Funcion que llama al Subject/Observable para cambiar el valor del filtro. Con todo el pedo de debounce and shit
     * @param filtro String. Valor actual del input
     */
    filtroChange(filtro: string) {
        this.filtroChanged.next(filtro);
    }

    /** Función que filtra filas de la tabla según el valor del "filtro" */
    aplicarFiltro() {
        // Verificando si hay algo en el filtro
        if (this.filtro) {
            const datosTabla: any[] = [];
            for (const dato of this.datos) {
                for (const column in dato) {
                    if (dato[column]) {
                        // checando si el valor del filtro se encuentra en el campo
                        if ( this.utils.remueveAcentosMinusculas(dato[column].value).indexOf(this.utils.remueveAcentosMinusculas(this.filtro)) !== -1 ) {
                            datosTabla.push(dato);
                            break;
                        }
                        // checando si tiene campo tuneado
                        if ( dato[column].htmlraw ) {
                            // checando si el valor del filtro se encuentra en el campo tuneado
                            if ( this.utils.remueveAcentosMinusculas(dato[column].htmlraw).indexOf(this.utils.remueveAcentosMinusculas(this.filtro)) !== -1 ) {
                                datosTabla.push(dato);
                                break;
                            }
                        }
                    }
                }
            }
            this.datosTabla = datosTabla;
            // Remover eventos
            this.eventos = this.removerEventos(this.eventos);
        } else {
            // Si no hay nada en el filtro acomodar la tabla por default
            this.generarPaginacion(1);
            // Remover eventos
            this.eventos = this.removerEventos(this.eventos);
        }
    }

    /** Función para dejar bien perra la páginación de la tabla con datos en memoria */
    generarPaginacion(pagina: number): void {
        try {
            // dividiendo la cantidad de datos entre el numero de registros a mostrar para calcular el número de páginas
            const paginas = Math.ceil(this.datos.length / this.controlCantidad);
            this.paginas = new Array(paginas);
            // pagina activa
            this.paginaActiva = pagina;
            // tomando la info a mostrar
            const inicio = (this.paginaActiva - 1) * this.controlCantidad;
            this.datosTabla = this.datos.slice(inicio, (this.controlCantidad * this.paginaActiva));
            // Acomodando botones de paginacion
            const helper = [];
            for (let i = (this.paginaActiva - 3); i <= (this.paginaActiva + 3); i++) {
                if (i > 0 && i <= this.paginas.length) {
                    helper.push(i);
                }
            }
            this.paginas = helper;
        } catch (err) {
            // console.log('Todavia no llegan los datos Async: ' + err.message);
        }
    }

    /**
     * Función para cambiar la pagina activa actual en la tabla
     * @param pagina numero de nueva página activa
     */
    cambiarPaginaActiva(pagina) {
        this.generarPaginacion(pagina);
        this.eventos = this.removerEventos(this.eventos);
    }

    /**
     * Función event emitter que puede ser llamada desde cualquier boton (u otro input) y hacerlo llegar al componente padre
     * Los botones que se generen desde el archivo config en columnDef deberían hacer referencia a este event emitter.
     * @param btn $event desde donde se llama la función
     */
    btnEmitEvent(btn) {
        // para inputs
        if (btn.srcElement.type === 'checkbox') {
            this.btnClickEvent.emit({
                'event': btn.srcElement,
                'tabActivo': this.tabActivo
            });
            return; // saliendo para que no mande un btn click falso
        }
        // para botones
        if (btn.srcElement.type !== 'button') {
            for (const element of btn.path) {
                if (element.type === 'button') {
                    this.btnClickEvent.emit({
                        'event': element,
                        'tabActivo': this.tabActivo
                    });
                    break;
                }
            }
        } else {
            this.btnClickEvent.emit({
                'event': btn.srcElement,
                'tabActivo': this.tabActivo
            });
        }
    }

    /**
     * Funcion para añadir event emitters a todos los elementos que matcheen un tag
     * @param tag tag html
     */
    addEventEmitterToTag(tag: string): void {
        // configurando events
        const elementos = document.getElementsByTagName(tag);
        for (let i = 0; i < elementos.length; i++) {
            const global = this.renderer.listen(elementos[i], 'click', (evt) => {
                this.btnEmitEvent(evt);
                // console.log(evt);
            });
            this.eventos.push(global);
        }
    }

    /**
     * Funcion para remover event emitters y borrar arreglo
     * @param eventos array de event emitters
     */
    removerEventos(eventos): any[] {
        for (const evento of eventos) {
            evento();
        }
        return [];
    }

    /**
     * Funcion para devolver el valor tuneado de un valor de columna. SIEMPRE NO LA USE
     * @param key key del array->objeto dato
     * @param value valor original
     */
    configColumnDef(key: any, value: any): SafeHtml {
        const res = this.config.columnDef[key](value);
        return this.sanitizer.bypassSecurityTrustHtml(res);
    }

    /**
     * Funcion para resetear datosTabla
     * La uso principalmente por que me deja mostrar el mensaje de "cargando datos..." cuando doy clic al boton refresh
     */
    datosTablaReset() {
        this.datosTabla = null;
    }

}
