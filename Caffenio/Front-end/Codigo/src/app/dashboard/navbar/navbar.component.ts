import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from './../../servicios/api.service';
import { AuthGuardService } from './../../servicios/auth-guard.service';

@Component({
    selector: 'caffenio-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

    // Rol del usuario
    user: any;
    // navbar collapse
    isCollapsed = true;

    constructor(private router: Router, private apiService: ApiService, private authGuardService: AuthGuardService) {}

    ngOnInit() {
        this.user = this.authGuardService.getUser();
    }

    ngOnDestroy() {}

    /** Función para cerrar sesión */
    cerrarsesion() {
        this.apiService.borrarToken();
        this.router.navigate(['/']);
    }

}
