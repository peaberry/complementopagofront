import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/** Modulos */
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { SharedModule } from '../shared/shared.module';

/** ng2 file upload */
import { FileUploadModule } from 'ng2-file-upload';

/** Componentes */
import { DashboardComponent } from './dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ComplementosComponent } from './complementos/complementos.component';
import { InvitacionesComponent } from './invitaciones/invitaciones.component';
import { NotificacionesComponent } from './notificaciones/notificaciones.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        // ngx-bootstrap
        CollapseModule,
        FileUploadModule,
        /** Mis modulos */
        SharedModule
    ],
    declarations: [
        DashboardComponent,
        SidebarComponent,
        NavbarComponent,
        ComplementosComponent,
        InvitacionesComponent,
        NotificacionesComponent
    ],
    exports: [
        DashboardComponent,
        SidebarComponent,
        NavbarComponent,
        ComplementosComponent,
        InvitacionesComponent,
        NotificacionesComponent
    ]
})
export class DashboardModule { }
