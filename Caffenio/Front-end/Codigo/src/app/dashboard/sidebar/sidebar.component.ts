import { Component, ElementRef, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { AuthGuardService } from './../../servicios/auth-guard.service';

@Component({
    selector: 'caffenio-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {

    // Para el toggle del sidebar expander/contraer
    @ViewChild('sidebar') sidebar: ElementRef;

    // Para los dropdown
    dropdowns: any = {};

    user: any;

    constructor(private authGuardService: AuthGuardService) {}

    ngOnInit() {
        if (window.screen.width <= 768) {
            this.sidebar.nativeElement.classList.add('collapsed');
        }
        // get user info
        this.user = this.authGuardService.getUser();
    }

    ngOnDestroy() {}

    /**
     * Función para togglear la sidebar cambiando la clase */
    toggle() {
        if (!this.sidebar.nativeElement.classList.value) {
            this.sidebar.nativeElement.classList.add('collapsed');
        } else {
            this.sidebar.nativeElement.classList.remove('collapsed');
        }
    }

    /** Función para los dropdowns de la sidebar  */
    toggleSideBarLinks(link) {
        // Cerrando todos
        for (const dropdown in this.dropdowns) {
            if (dropdown !== link) {
                this.dropdowns[dropdown] = false;
            }
        }
        // Abriendo el que clickearon
        this.dropdowns[link] = !this.dropdowns[link];
    }

}
