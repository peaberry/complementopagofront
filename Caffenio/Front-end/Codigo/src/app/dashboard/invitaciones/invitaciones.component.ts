import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import swal from 'sweetalert2';

import { ApiService } from './../../servicios/api.service';

@Component({
    selector: 'caffenio-invitaciones',
    templateUrl: './invitaciones.component.html',
    styleUrls: ['./invitaciones.component.css']
})
export class InvitacionesComponent implements OnInit {

    // forma
    filtroForm: FormGroup = new FormGroup({});
    companies = [
        {
            'CompanyId': 'OSLO',
            'CompanyName': 'Servicios Adminsitrativos OSLO'
        },
        {
            'CompanyId': 'CAFEP',
            'CompanyName': 'CAFEPAC SA de CV'
        }
    ];

    // tabla
    columnas: string[] = ['ID', 'Nombre', 'Correo', 'RFC', 'Seleccionar'];
    datos: any[];
    configDataTable: any = {
        columnDef: {
            'seleccionar': function(value): string {
                return `
                    <div class="form-check">
                        <input class="form-check-input btn-checkbox" style="padding-left:0px;" type="checkbox" data-id="${value.ProviderId.value}" ${(value['seleccionar'] ? 'checked' : '')}>
                        <label class="form-check-label" style="padding-left:0px;" for="defaultCheck1">Envíar correo</label>
                    </div>
                `;
            }
        },
        columnStyle: {
            'opciones': function(): any {
                return {
                    'white-space': 'nowrap'
                };
            }
        }
    };

    constructor(private fbuilder: FormBuilder, private apiService: ApiService) {}

    ngOnInit() {
        this.obtenerCompanies();
        this.iniciarForma();
    }

    /** Funciona para iniciar la forma de filtro */
    iniciarForma() {
        this.filtroForm = this.fbuilder.group({
            rfc: ['', [
                Validators.required,
                Validators.pattern(/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/)
            ]],
            company: ['', [Validators.required]],
        });
    }

    async obtenerCompanies() {
        try {
            const comp = await this.apiService.GET('CaffenioCompanies/Get', 'caffenio', false, false, 'body');
            this.companies = comp;
        } catch (err) {
            console.error(err);
        }
    }

    /**
     * Funcion para obtener la lista de correos / proveedores a invitar
     * @param post
     */
    async submit(post) {
        const self = this;
        if (this.filtroForm.valid) {
            // Loading sweetalert
            swal({
                html: `
                    <img src="assets/loading-1.5s.gif" alt="loading">
                    <h2 style="color: #595959;font-size: 30px;text-align: center;font-weight: 600;text-transform: none;position: relative;margin: 0 0 .4em;padding: 0;display: block;word-wrap: break-word;">
                        Espere un momento...
                    </h2>
                    <div style="font-size: 18px;text-align: center;font-weight: 300;position: relative;float: none;margin: 0;padding: 0;line-height: normal;color: #545454;word-wrap: break-word;">
                        Procesando solicitud
                    </div>
                `,
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showConfirmButton: false
            });
            // llamada al api
            try {
                const resp = await this.apiService.GET(`Provider/GetbyRFC?RFC=${post.rfc}&CompanyId=${post.company}`, 'caffenio', false, false, 'body');
                // fake
                /* const resp = [
                    {
                        'ProviderId': 'PRV0000001',
                        'ProviderName': 'provedor uno',
                        'RFC': 'CPA030421M70',
                        'Email': 'jlvb93@gmail.com'
                    },
                    {
                        'ProviderId': 'PRV0000002',
                        'ProviderName': 'provedor dos',
                        'RFC': 'CPA030421M70',
                        'Email': 'provedor2@proveedor.com'
                    }
                ]; */   
                // configurando datos
                for (const prov of resp) {
                    prov['seleccionar'] = true;                   
                }
                // pasando datos a la interfaz
                this.datos = resp;
                swal.close();
            } catch (err) {
                console.log(err);
                swal( 'Ocurrio un error.', 'Error con el servidor o RFC no registrado en el sistema.', 'error' );
            }
        } else {
            const controls = this.filtroForm.controls;
            for (const control in controls) {
                if (!controls[control].valid) {
                    controls[control].markAsTouched();
                }
            }
        }
    }

    /** Funcion para enviar los correos una vez traidos del server caffenio */
    async send_mail(selected_company) {
        try {
            // Loading sweetalert
            swal({
                html: `
                    <img src="assets/loading-1.5s.gif" alt="loading">
                    <h2 style="color: #595959;font-size: 30px;text-align: center;font-weight: 600;text-transform: none;position: relative;margin: 0 0 .4em;padding: 0;display: block;word-wrap: break-word;">
                        Espere un momento...
                    </h2>
                    <div style="font-size: 18px;text-align: center;font-weight: 300;position: relative;float: none;margin: 0;padding: 0;line-height: normal;color: #545454;word-wrap: break-word;">
                        Procesando solicitud
                    </div>
                `,
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showConfirmButton: false
            });
            // enviando correos
            const correos = [];            
            for (const correo of this.datos) {
                if (correo.seleccionar.value) {
                    const c = {                        
                        providerId: correo.ProviderId.value,
                        providerName: correo.ProviderName.value,
                        rfc: correo.RFC.value,
                        email: correo.Email.value,
                        companyId: selected_company,
                        user: {
                            username: correo.RFC.value,
                            email: correo.Email.value,
                            password: correo.ProviderId.value, 
                            is_staff: false,                           
                        },
                    };
                    correos.push(c);
                }
            }
            const resp = await this.apiService.POST(
                'invitaciones/',
                {emails: correos}, 'ro', false, false, true, 'body'
            );
            // limpiando al final
            swal('Accion exitosa', 'Las invitaciones han sido enviadas con exito.', 'success');
            this.datos = [];
            this.filtroForm.reset();
        } catch (err) {
            console.error(err);
            swal( 'Ocurrio un error.', 'Error al enviar las invitaciones. Por favor intente más tarde.', 'error' );
        }
    }

    /**
     * Funcion para los botones de la tabla
     * @param event
     */
    dataTableBtnEvent(event) {
        // Tomando el id del row. Asumiendo que esta contenido en el data-id del boton
        let id; const self = this;
        try {
            id = event.event.attributes['data-id'].nodeValue;
        } catch (err) { /* console.log('No cuenta con data-id: ' + err.message); */ }

        if (event.event.classList.contains('btn-checkbox')) {
            let chckbx = self.datos.find(x => x.ProviderId.value === id).seleccionar.value;
            chckbx = !chckbx;
            self.datos.find(x => x.ProviderId.value === id).seleccionar.value = chckbx;
        }
    }

    /** Forma para verificar que existen correos seleccionados para el envio de invitaciones */
    correosSeleccionados() {
        const verificacion = (this.datos) ? this.datos.find(x => x.seleccionar.value === true) : undefined;
        if (!verificacion) {
            return true;
        }
        return null;
    }

}
