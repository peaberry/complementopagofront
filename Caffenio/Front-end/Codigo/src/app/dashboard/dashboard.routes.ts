import { Routes } from '@angular/router';

/** Componentes */
import { ComplementosComponent } from './complementos/complementos.component';
import { InvitacionesComponent } from './invitaciones/invitaciones.component';
import { NotificacionesComponent } from './notificaciones/notificaciones.component';

export const dashboardRoutes: Routes = [
    { path: 'complementos', component: ComplementosComponent },
    { path: 'invitaciones', component: InvitacionesComponent },
    { path: 'notificaciones', component: NotificacionesComponent },
    { path: '', redirectTo: 'complementos', pathMatch: 'full' },
    { path: '**', redirectTo: 'complementos' }
];
