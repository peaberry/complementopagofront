import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import swal from 'sweetalert2';

import { ApiService } from './../../servicios/api.service';

@Component({
    selector: 'caffenio-notificaciones',
    templateUrl: './notificaciones.component.html',
    styleUrls: ['./notificaciones.component.css']
})
export class NotificacionesComponent implements OnInit {

    // forma
    filtroForm: FormGroup = new FormGroup({});
    companies = [
        {
            'CompanyId': 'OSLO',
            'CompanyName': 'Servicios Adminsitrativos OSLO'
        },
        {
            'CompanyId': 'CAFEP',
            'CompanyName': 'CAFEPAC SA de CV'
        }
    ];

    // tabla
    columnas: string[] = ['Id', 'Correo', 'Compañia', 'Opciones'];
    datos: any[];
    configDataTable: any = {
        columnDef: {
            'opciones': function(value): string {
                return `
                    <button type="button" title="Eliminar" class="btn btn-outline-danger btn-sm btn-eliminar" data-id="${value.id.value}">
                        <i class="fas fa-trash"></i> Eliminar
                    </button>
                `;
            }
        },
        columnStyle: {
            'opciones': function(): any {
                return {
                    'white-space': 'nowrap',
                    'width': '15%'
                };
            }
        }
    };

    constructor(private fbuilder: FormBuilder, private apiService: ApiService) {}

    ngOnInit() {
        this.obtenerCompanies();
        this.iniciarForma();
    }

    /** Funciona para iniciar la forma de filtro */
    iniciarForma() {
        this.filtroForm = this.fbuilder.group({
            company: ['', [Validators.required]],
            email: ['', [
                Validators.maxLength(255),
                Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
            ]],
        });
    }

    /** funcion para obtener compañias */
    async obtenerCompanies() {
        try {
            const comp = await this.apiService.GET('CaffenioCompanies/Get', 'caffenio', false, false, 'body');
            this.companies = comp;
        } catch (err) {
            console.error(err);
        }
    }

    /**
     *  Función para añadir un correo a la lista de notificaciones
     * @param post
     */
    async submit(post) {
        const self = this;
        // Loading sweetalert
        swal({
            html: `
                <img src="assets/loading-1.5s.gif" alt="loading">
                <h2 style="color: #595959;font-size: 30px;text-align: center;font-weight: 600;text-transform: none;position: relative;margin: 0 0 .4em;padding: 0;display: block;word-wrap: break-word;">
                    Espere un momento...
                </h2>
                <div style="font-size: 18px;text-align: center;font-weight: 300;position: relative;float: none;margin: 0;padding: 0;line-height: normal;color: #545454;word-wrap: break-word;">
                    Procesando solicitud
                </div>
            `,
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            showConfirmButton: false
        });
        // llamada al api
        try {
            const resp = await this.apiService.POST(
                'notificaciones/',
                post, 'ro', false, false, true, 'body'
            );
            this.filtroForm.get('email').reset();
            await this.companyOnChange(this.filtroForm.get('company').value);
            swal('Accion exitosa', 'Correo añadido a la lista de notificaciones con exito.', 'success');
        } catch (err) {
            console.log(err);
            let msg = '';
            if (err.error.non_field_errors) {
                msg = 'El correo ya se encuentra registrado para la compañia.';
            } else {
                msg = 'Ocurrio un error inesperado. Por favor intente más tarde.';
            }
            swal( 'Ocurrio un error.', msg, 'error' );
        }
    }

    /**
     * Funcion para obtener correos registrados de X compañia
     * @param value
     */
    async companyOnChange(value) {
        try {
            // Loading sweetalert
            swal({
                html: `
                    <img src="assets/loading-1.5s.gif" alt="loading">
                    <h2 style="color: #595959;font-size: 30px;text-align: center;font-weight: 600;text-transform: none;position: relative;margin: 0 0 .4em;padding: 0;display: block;word-wrap: break-word;">
                        Espere un momento...
                    </h2>
                    <div style="font-size: 18px;text-align: center;font-weight: 300;position: relative;float: none;margin: 0;padding: 0;line-height: normal;color: #545454;word-wrap: break-word;">
                        Procesando solicitud
                    </div>
                `,
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showConfirmButton: false
            });
            // trayendo valores
            const resp = await this.apiService.GET(
                `notificaciones/company/${value}/`,
                'ro', false, true, 'body'
            );
            // acomodando valores
            for (const email of resp) {
                email['opciones'] = 'lmao';
            }
            this.datos = resp;
            swal.close();
        } catch (err) {
            this.datos = [];
            swal.close();
            console.error(err);
        }
    }

    /**
     * Funcion para los botones de la tabla
     * @param event
     */
    dataTableBtnEvent(event) {
        // Tomando el id del row. Asumiendo que esta contenido en el data-id del boton
        let id; const self = this;
        try {
            id = event.event.attributes['data-id'].nodeValue;
        } catch (err) { /* console.log('No cuenta con data-id: ' + err.message); */ }

        // Accion dependiendo del evento. El tipo de boton se define por la clase que contiene
        if (event.event.classList.contains('btn-eliminar')) {
            swal({
                title: '¿Eliminar correo?',
                text: `El correo ya no recibira notificaciones`,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#82bc00',
                cancelButtonColor: '#f42434',
                confirmButtonText: 'Eliminar',
                cancelButtonText: 'Cancelar'
            }).then(async function (result) {
                if (result.value) {
                    // swal de cargando / procesando
                    swal({
                        html: `
                            <img src="assets/loading-1.5s.gif" alt="loading">
                            <h2 style="color: #595959;font-size: 30px;text-align: center;font-weight: 600;text-transform: none;position: relative;margin: 0 0 .4em;padding: 0;display: block;word-wrap: break-word;">
                                Espere un momento...
                            </h2>
                            <div style="font-size: 18px;text-align: center;font-weight: 300;position: relative;float: none;margin: 0;padding: 0;line-height: normal;color: #545454;word-wrap: break-word;">
                                Procesando solicitud
                            </div>
                        `,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        showConfirmButton: false
                    });
                    // llamada al api
                    try {
                        const resp = await self.apiService.DELETE(
                            `notificaciones/${id}/`,
                            'ro', false, true, 'body'
                        );
                        await self.companyOnChange(self.filtroForm.get('company').value);
                        swal('Acción exitosa', 'El correo ha sido eliminado.', 'success');
                    } catch (err) {
                        swal('Ocurrio un error', 'Por favor intente de nuevo más tarde.', 'error');
                    }
                }
            }).catch(function (err) {
                swal('Ocurrio un error', 'Por favor intente de nuevo más tarde.', 'error');
            });
        }
    }

}
