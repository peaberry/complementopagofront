import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';
import { FileUploader } from 'ng2-file-upload';
import swal from 'sweetalert2';
import * as xml2js from 'xml2js';

import { ApiService } from './../../servicios/api.service';
import { AuthGuardService } from './../../servicios/auth-guard.service';

@Component({
    selector: 'caffenio-complementos',
    templateUrl: './complementos.component.html',
    styleUrls: ['./complementos.component.css']
})
export class ComplementosComponent implements OnInit {

    /** Fileuploader */
    uploader: FileUploader = new FileUploader({
        url:  'https://ayylmao.com', // URL de subida (no se utilizo / requisito para el objeto base)
        isHTML5: true,
        allowedMimeType: ['application/pdf', 'text/xml'],
        /* queueLimit: 2, */
    });
    hasBaseDropZoneOver = false;

    /** Filtro de compañia */
    user: any; // información del usuario logeado
    validacionForm: FormGroup = new FormGroup({});
    get Payments() { return <FormArray>this.validacionForm.get('Payments'); }
    companies = [
        {
            'CompanyId': 'OSLO',
            'CompanyName': 'Servicios Adminsitrativos OSLO'
        },
        {
            'CompanyId': 'CAFEP',
            'CompanyName': 'CAFEPAC SA de CV'
        }
    ];

    /** CFDIs */
    complementos: any[] = [];
    complementosValidos: any[] = [];
    CFDIs: any[];
    // Tabla CFDIs
    columnas: string[] = ['Factura', 'Complemento', 'Pago Valido', 'Pago completado', 'Total factura', 'Pago complemento', 'Adeudo'];
    datos: any[];
    configDataTable: any = {
        columnDef: {
            'valido': function(value): string {
                return (value['valido']) ?
                '<span class="badge badge-success" style="font-weight: normal;padding-left: 15px;padding-right: 15px;">Valido</span>' :
                '<span class="badge badge-danger" style="font-weight: normal;padding-left: 15px;padding-right: 15px;">Invalido</span>';
            },
            'completado': function(value): string {
                return (value['completado']) ?
                '<span class="badge badge-success" style="font-weight: normal;padding-left: 15px;padding-right: 15px;">Pago completo</span>' :
                '<span class="badge badge-warning" style="font-weight: normal;padding-left: 15px;padding-right: 15px;">Pendiente</span>';
            },
            'amount': function(value): string {
                return '$' + value['amount'].toFixed(2);
            },
            'paymentamount': function(value): string {
                return '$' + value['paymentamount'].toFixed(2);
            },
            'restante': function(value): string {
                return '$' + value['restante'].toFixed(2);
            }
        },
        columnStyle: {}
    };

    constructor(private apiService: ApiService, private authGuardService: AuthGuardService, private fbuilder: FormBuilder, private http: HttpClient) {}

    ngOnInit() {
        // Obtener info del usuario logeado
        this.user = this.authGuardService.getUser();
        // Para el filtro de compañia
        this.obtenerCompanies();
        this.iniciarForma();
        /** Se dispara al seleccionar un archivo en el file-uploader */
        this.uploader.onAfterAddingFile = async (file) => {
            const self = this;
            // No permitir dos veces el mismo archivo
            const repetido = this.uploader.queue.filter(x => x.file.name === file.file.name).length;
            if (repetido > 1) {
                this.uploader.removeFromQueue(file);
                return;
            }
            // Parseando XML
            if (file.file.type === 'text/xml') {
                try {
                    const complemento = {};
                    // convirtiendo file a string text/xml
                    const texto = await this.getXMLText(file.file.rawFile);
                    // sacando datos del XML
                    xml2js.parseString(texto, function (err, result) {
                        result['filename'] = file.file.name;
                        self.complementos.push(result);
                        complemento['UUIDRelated'] = result['cfdi:Comprobante']['cfdi:Complemento'][0]['tfd:TimbreFiscalDigital'][0]['$']['UUID'];
                        complemento['PaymentAmount'] = result['cfdi:Comprobante']['$']['Total'];
                        /* complemento['Folio'] = result['cfdi:Comprobante']['$']['Folio']; */
                    });
                    // convirtiendo xml string a base64
                    complemento['XMLBase64'] = btoa(texto as string);
                    // Pasando valor al arreglo de la forma
                    this.Payments.push(this.fbuilder.group({
                        UUIDRelated: [complemento['UUIDRelated'], [Validators.required]],
                        PaymentAmount: [complemento['PaymentAmount'], [Validators.required]],
                        XMLBase64: [complemento['XMLBase64'], [Validators.required]]
                    }));
                } catch (err) {
                    swal( 'Ocurrio un error.', 'El documento XML no parece ser una factura valida.', 'error' );
                    this.uploader.removeFromQueue(file);
                }
            }
        };
    }

    /** Funcion para verificar que los documentos seleccionados cumplan con ser pares */
    comprobarPares(): Boolean {
        try {
            const XMLs = this.uploader.queue.filter(x => x.file.type === 'text/xml');
            const PDFs = this.uploader.queue.filter(x => x.file.type === 'application/pdf');
            if (XMLs.length !== PDFs.length) {
                return false;
            }
            for (const xml of XMLs) {
                const match = PDFs.find(x => x.file.name.substr(0, x.file.name.lastIndexOf('.')) === xml.file.name.substr(0, xml.file.name.lastIndexOf('.')));
                if (!match) { return false; }
            }
            return true;
        } catch (err) {
            console.error(err);
        }
    }

    fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    /**
     * Funcion para obtener texto de un archivo XML
     * @param file archivo XML como string ?
     */
    getXMLText(file) {
        return new Promise(function (resolve, reject) {
            try {
                const reader = new FileReader();
                reader.onload = function() {
                    const text = reader.result;
                    resolve(text);
                };
                reader.readAsText(new Blob([file]));
            } catch (err) {
                console.error(err);
                reject(err);
            }
        });
    }

    /** Funcion para construir el formgroup del filtro por compañia */
    iniciarForma() {
        this.validacionForm = this.fbuilder.group({
            ProviderId: [(this.user) ? this.user.providerId : '', [Validators.required]],
            CompanyId: ['', [Validators.required]],
            Payments: this.fbuilder.array([])
        });
    }

    /** Funcion para limpiar la forma de validacion completamente */
    limpiarForma() {
        this.validacionForm.reset();
        this.validacionForm.get('ProviderId').setValue(this.user.providerId);
        while (this.Payments.length !== 0) {
            this.Payments.removeAt(0);
        }
    }

    /** Funcion para obtener lista de compañias desde el backend */
    async obtenerCompanies() {
        try {
            const comp = await this.apiService.GET('CaffenioCompanies/Get', 'caffenio', false, false, 'body');
            this.companies = comp;
        } catch (err) {
            console.error(err);
        }
    }

    /** Funcion para la validacion de complementos */
    async comenzarValidacion() {
        try {
            // Loading sweetalert
            swal({
                html: `
                    <img src="assets/loading-1.5s.gif" alt="loading">
                    <h2 style="color: #595959;font-size: 30px;text-align: center;font-weight: 600;text-transform: none;position: relative;margin: 0 0 .4em;padding: 0;display: block;word-wrap: break-word;">
                        Espere un momento...
                    </h2>
                    <div style="font-size: 18px;text-align: center;font-weight: 300;position: relative;float: none;margin: 0;padding: 0;line-height: normal;color: #545454;word-wrap: break-word;">
                        Procesando solicitud
                    </div>
                `,
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showConfirmButton: false
            });
            // Llamada para validar las facturas con Caffenio
            this.CFDIs = await this.apiService.POST(
                'CFDIProviderHeader/Validate',
                this.validacionForm.value,
                'caffenio',
                false,
                false,
                false,
                'body'
            );
            // Simulado
            /* this.CFDIs = [
                {
                    'DocNumber': 'DOC000156',
                    'UUID': 'e5567-e99b-12d3-a456-426655440000',
                    'Amount': 100.00,
                    'UUIDRelated': '91A79F69-6F80-41A4-8543-015C364B4720',
                    'PaymentAmount': 100.00,
                    'IsPaymentValid': true,
                    'IsPaymentComplete': true
                },
                {
                    'DocNumber': 'DOC000250',
                    'UUID': 'e5567-e99b-12d3-a456-426655440001',
                    'Amount': 150.00,
                    'UUIDRelated': '8754B049-A47F-466F-B8DC-AFB2299AF2D9',
                    'PaymentAmount': 100.00,
                    'IsPaymentValid': true,
                    'IsPaymentComplete': false
                },
                {
                    'DocNumber': 'DOC000198',
                    'UUID': 'e5567-e99b-12d3-a456-426655440002',
                    'Amount': 100.00,
                    'UUIDRelated': '684068E1-D637-486E-AE06-25BAF10CCCC4',
                    'PaymentAmount': 100.00,
                    'IsPaymentValid': true,
                    'IsPaymentComplete': true
                }
            ]; */
            this.configurarDatos(this.CFDIs);
            swal.close();
        } catch (err) {
            console.error(err);
            swal( 'Ocurrio un error.', 'Error al validar el complemento. Por favor intente de nuevo más tarde.', 'error' );
        }
    }

    /**
     * Funcion para configurar los datos que vienen de la validación de complementos y mostrarlos en la tabla
     * @param CFDIs ayy
     */
    async configurarDatos(CFDIs) {
        try {
            const datosTabla = [];
            for (const cfdi of CFDIs) {
                const complemento = this.complementos.find(x => x['cfdi:Comprobante']['cfdi:Complemento'][0]['tfd:TimbreFiscalDigital'][0]['$']['UUID'] === cfdi.UUIDRelated);
                if (complemento) {
                    this.complementosValidos.push(complemento);
                    const c = {
                        factura: cfdi.DocNumber,
                        complemento: complemento.filename,
                        valido: cfdi.IsPaymentValid,
                        completado: cfdi.IsPaymentComplete,
                        amount: cfdi.Amount,
                        paymentamount: cfdi.PaymentAmount,
                        restante: cfdi.Amount - cfdi.PaymentAmount
                    };
                    datosTabla.push(c);
                }
            }
            this.datos = datosTabla;
        } catch (err) {
            console.error(err);
        }
    }

    /** Funcion para reiniciar el estado de todo el proceso */
    limpiarTodo() {
        this.limpiarForma();
        this.datos = undefined;
        this.complementos = [];
        this.CFDIs = undefined;
        this.uploader.clearQueue();
    }

    async subirComplementos() {
        try {
            const self = this;
            /** Carga de multiples archivos */
            // Modal de carga
            swal({
                html: `
                    <img src="assets/loading-1.5s.gif" alt="loading">
                    <h2 style="color: #595959;font-size: 30px;text-align: center;font-weight: 600;text-transform: none;position: relative;margin: 0 0 .4em;padding: 0;display: block;word-wrap: break-word;">
                        Espere un momento...
                    </h2>
                    <div style="font-size: 18px;text-align: center;font-weight: 300;position: relative;float: none;margin: 0;padding: 0;line-height: normal;color: #545454;word-wrap: break-word;">
                        Procesando solicitud y subiendo archivos.
                    </div>
                    <div class="progress" style="margin-top:15px;">
                        <div id="progress-bar" class="progress-bar" role="progressbar" style="width:0%;"></div>
                    </div>
                `,
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showConfirmButton: false
            });

            // Obteniendo los archivos
            const XMLs = this.uploader.queue.filter(x => x.file.type === 'text/xml');
            const PDFs = this.uploader.queue.filter(x => x.file.type === 'application/pdf');
            const formModels = [];

            // Haciendo pares
            for (const xml of XMLs) {
                const match = this.complementosValidos.find(x => x.filename === xml.file.name); // match con documentos validados
                if (match) {
                    const pdf = PDFs.find(x => x.file.name.substr(0, x.file.name.lastIndexOf('.')) === xml.file.name.substr(0, xml.file.name.lastIndexOf('.')));
                    const formModel = new FormData();
                    formModel.append('xml', xml.file.rawFile);
                    formModel.append('pdf', pdf.file.rawFile);
                    formModel.append('company', this.validacionForm.value.CompanyId);
                    formModel.append('folio', match['cfdi:Comprobante']['$']['Folio']);
                    formModel.append('desc', 'Agregados desde el sistema web.');
                    formModels.push(formModel);
                }
            }

            // Haciendo envios
            for (const model of formModels) {
                await new Promise(function(resolve, reject) {
                    // call
                    const resp = self.apiService.POSTprogress(
                        `complementos/`,
                        model, 'ro', false, true, true, 'body'
                    );
                    // Porcentaje
                    self.http.request(resp).subscribe(event => {
                        if (event.type === HttpEventType.UploadProgress) {
                            const percentage = Math.round(100 * event.loaded / event.total);
                            document.getElementById('progress-bar').setAttribute('style', `width:${percentage}%;background-color: #82bc00`);
                        } if (event instanceof HttpResponse) {
                            resolve();
                        }
                    });
                });
            }

            swal('Accion exitosa', 'Los complementos han sido guardados.', 'success').then(() => {
                self.limpiarTodo();
            });
        } catch (err) {
            console.error(err);
            swal( 'Ocurrio un error.', 'Por favor intente de nuevo más tarde.', 'error' );
        }
    }

}
