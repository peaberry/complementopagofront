import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Routing } from './app.routing';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

/** Servicios */
import { UtilsService } from './servicios/utils.service';
import { ValidatorsService } from './servicios/validators.service';
import { SessionService } from './servicios/session.service';
import { AuthGuardService } from './servicios/auth-guard.service';
import { CuatroCeroTresComponent } from './noDashboard/403/403.component';

/** Mis modulos */
import { DashboardModule } from './dashboard/dashboard.module';
import { NoDashboardModule } from './noDashboard/noDashboard.module';
import { SharedModule } from './shared/shared.module';
import { ApiService } from './servicios/api.service';

@NgModule({
  declarations: [
    AppComponent,
    CuatroCeroTresComponent
  ],
  imports: [
    BrowserModule,
    Routing,
    RouterModule,
    HttpClientModule,
    /** Mis modulos */
    DashboardModule,
    NoDashboardModule,
    SharedModule
  ],
  providers: [ UtilsService, ValidatorsService, ApiService, SessionService, AuthGuardService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
