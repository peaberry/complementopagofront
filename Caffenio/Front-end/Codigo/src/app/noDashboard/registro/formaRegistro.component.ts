import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

import { ValidatorsService } from './../../servicios/validators.service';
import { ApiService } from '../../servicios/api.service';


@Component({
    selector: 'caffenio-formaregistro',
    templateUrl: './formaRegistro.component.html',
    styleUrls: ['./formaRegistro.component.css']
})
export class FormaRegistroComponent implements OnInit {

    /** Info de la forma */
    registroForm: FormGroup = new FormGroup({});
    companies = [
        {
            'CompanyId': '4',
            'CompanyName': 'Servicios Adminsitrativos OSLO'
        },
        {
            'CompanyId': '2',
            'CompanyName': 'CAFEPAC SA de CV'
        }
    ];

    constructor(private fbuilder: FormBuilder, private customValidators: ValidatorsService,
        private apiService: ApiService, private router: Router) {}

    ngOnInit() {
        // Iniciando la forma de usuario
        this.obtenerCompanies();
        this.iniciarForma();
    }

    /** Funcion para iniciar la forma de usuario */
    iniciarForma() {
        // this.usuarioForm = new FormGroup({});
        this.registroForm = this.fbuilder.group({
            rfc: ['', [
                Validators.required,
                Validators.pattern(/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/)
            ]],
            email: ['', [
                Validators.required, Validators.maxLength(255),
                Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
            ]],
            password: ['', [
                Validators.required, Validators.minLength(6)
            ]],
            password2: ['', [
                Validators.required,
                (control) => this.customValidators.matchEmail(control, this.registroForm.get('password') as FormControl)
            ]],
            company: ['', [Validators.required,]],
        });
    }
    
    async obtenerCompanies() {
        try {
            const comp = await this.apiService.GET('CaffenioCompanies/Get', 'caffenio', false, false, 'body');
            this.companies = comp;
        } catch (err) {
            console.error(err);
        }
    }

    /**
     * Función para hacer la petición de registro/update al servidor
     * @param post info de la forma
     */
    async submit(post) {
        const self = this;
        if (this.registroForm.valid) {
            // Loading sweetalert
            swal({
                html: `
                    <img src="assets/loading-1.5s.gif" alt="loading">
                    <h2 style="color: #595959;font-size: 30px;text-align: center;font-weight: 600;text-transform: none;position: relative;margin: 0 0 .4em;padding: 0;display: block;word-wrap: break-word;">
                        Espere un momento...
                    </h2>
                    <div style="font-size: 18px;text-align: center;font-weight: 300;position: relative;float: none;margin: 0;padding: 0;line-height: normal;color: #545454;word-wrap: break-word;">
                        Procesando solicitud
                    </div>
                `,
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showConfirmButton: false
            });
            // llamada al api
            try {
                // caffenio validation
                const prov = await this.apiService.POST(
                    'Provider/Validate',
                    {
                        ProviderId: '',
                        ProviderName: '',
                        RFC: post.rfc,
                        Email: post.email,
                        CompanyId: post.company,
                    },
                    'caffenio',
                    false,
                    false,
                    false,
                    'body'
                );
                // fake response
                /* const prov = {
                    ProviderId: 'PRV0000002',
                    ProviderName: 'Jose Luis Vega',
                    RFC: 'SAO030421M71',
                    Email: 'wafo@gmail.com'
                }; */
                // registro
                const resp = await this.apiService.POST(
                    `caffeniousers/register/`,
                    {
                        user: {
                            username: post.rfc,
                            password: post.password,
                            email: post.email,                            
                        },
                        providerId: prov.ProviderId,
                        providerName: prov.ProviderName,
                        rfc: post.rfc,
                        companyId: post.company,
                    },
                    'ro', false, false, false, 'body'
                );
                // exito / redirigiendo
                swal(
                    'Exito',
                    'Se ha registrado con exito. Ya puede acceder al sistema.',
                    'success'
                ).then(() => {
                    this.router.navigate(['/login']);
                });
            } catch (err) {
                console.log(err);
                let errmsg = '';
                if (err.status === 400) {
                    if (err.error.user && err.error.user.username) { errmsg += err.error.user.username + ' '; }
                    if (err.error.user && err.error.user.email) { errmsg += 'Esta dirección de correo ya esta en uso. '; }
                    if (err.error.providerId) { errmsg += err.error.providerId + ' '; }
                    if (err.error.rfc) { errmsg += err.error.rfc + ' '; }
                } else {
                    errmsg = 'Error inesperado. Por favor intente más tarde.';
                }
                swal( 'Ocurrio un error.', `Ha ocurrido un error: ${errmsg}`, 'error' );
            }
        } else {
            const controls = this.registroForm.controls;
            for (const control in controls) {
                if (!controls[control].valid) {
                    controls[control].markAsTouched();
                }
            }
        }
    }

}
