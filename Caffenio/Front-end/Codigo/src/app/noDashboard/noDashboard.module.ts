import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
// Reactive Forms
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/** Modulos */
import { SharedModule } from '../shared/shared.module';

/** Componentes */
import { NoDashboardComponent } from './noDashboard.component';
import { LoginComponent } from './login/login.component';
import { FormaRegistroComponent } from './registro/formaRegistro.component';
import { RestaurarPassComponent } from './restaurarPassword/restaurarPass.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        /** Mis modulos */
        SharedModule
    ],
    declarations: [
        NoDashboardComponent,
        LoginComponent,
        FormaRegistroComponent,
        RestaurarPassComponent
    ],
    exports: [
        NoDashboardComponent,
        LoginComponent,
        FormaRegistroComponent,
        RestaurarPassComponent
    ]
})
export class NoDashboardModule { }
