import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

import { ApiService } from './../../servicios/api.service';
import { SessionService } from './../../servicios/session.service';
import { AuthGuardService } from '../../servicios/auth-guard.service';

@Component({
    selector: 'caffenio-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    // Login form
    loginForm: FormGroup;
    recuperarForm: FormGroup;
    formaMostrar = 1;

    constructor (private fbuilder: FormBuilder, private router: Router, private apiService: ApiService, private sessionService: SessionService
        , private authGuardService: AuthGuardService) {}

    ngOnInit() {
        // Verificando si ya tiene sesión
        if (this.sessionService.retrieveToken()) {
            this.router.navigate(['/dashboard']);
        }
        // Iniciando forma
        this.iniciarForma();
        this.iniciarFormaRecuperar();
    }

    iniciarForma() {
        this.loginForm = this.fbuilder.group({
            email: ['', [
                Validators.required,
                Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
            ]],
            password: ['', [
                Validators.required,
                Validators.minLength(6)
            ]]
        });
    }

    iniciarFormaRecuperar() {
        this.recuperarForm = this.fbuilder.group({
            email: ['', [
                Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
            ]],
            rfc: ['', [
                Validators.pattern(/^([A-Z,Ñ,&]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3})$/)
            ]]
        });
    }

    /**
     * Funcion para iniciar sesión
     * @param post valores de la forma de login
     */
    async login(post) {
        if (this.loginForm.valid) {
            // Loading sweetalert
            swal({
                html: `
                    <img src="assets/loading-1.5s.gif" alt="loading">
                    <h2 style="color: #595959;font-size: 30px;text-align: center;font-weight: 600;text-transform: none;position: relative;margin: 0 0 .4em;padding: 0;display: block;word-wrap: break-word;">
                        Espere un momento...
                    </h2>
                    <div style="font-size: 18px;text-align: center;font-weight: 300;position: relative;float: none;margin: 0;padding: 0;line-height: normal;color: #545454;word-wrap: break-word;">
                        Iniciando sesión
                    </div>
                `,
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showConfirmButton: false
            });
            try {
                // Login en backend RO
                const token = await this.sessionService.generateNewToken(post.email, post.password);
                await this.authGuardService.tokenValido();
                const user = this.authGuardService.getUser();
                // Verificar validez de usuario con caffenio
                if (!user.user.is_staff) {
                    console.log("entro is not staff");
                    const valido = await this.apiService.GET(`User/Validate?providerId=${user.providerId}&companyId=${user.companyId}`, 'caffenio', false, false, 'body');
                    if (!valido) {
                        swal(
                            'Hubo un problema',
                            'El correo no pertenece a una cuenta valida del sistema. Por favor comunícate con nosotros.',
                            'warning'
                        );
                        this.sessionService.borrarToken();
                    }
                }
                swal.close();
                this.router.navigate(['/dashboard']);
            } catch (err) {
                let msg = '';
                if(err.status === 401){
                    this.sessionService.borrarToken();
                    this.login(post);
                    return;
                }
                if (err.error.non_field_errors) {
                    msg = 'Nombre de usuario o contraseña incorrectos.';
                } else {
                    msg = 'Error inesperado, intente más tarde o comuníquese con nosotros para ayudarlo.';
                }
                swal(
                    'Hubo un problema',
                    msg,
                    'warning'
                );
                console.error(err);
            }
        } else {
            // Tocando todos los controles para mostrar los errores
            const controls = this.loginForm.controls;
            for (const control in controls) {
                if (!controls[control].valid) {
                    controls[control].markAsTouched();
                }
            }
        }
    }

    /**
     * Funcion para recuperar contraseña
     * @param post valores de la forma
     */
    async recuperar(post) {
        if (this.recuperarForm.valid) {
            try {
                // Loading sweetalert
                swal({
                    html: `
                        <img src="assets/loading-1.5s.gif" alt="loading">
                        <h2 style="color: #595959;font-size: 30px;text-align: center;font-weight: 600;text-transform: none;position: relative;margin: 0 0 .4em;padding: 0;display: block;word-wrap: break-word;">
                            Espere un momento...
                        </h2>
                        <div style="font-size: 18px;text-align: center;font-weight: 300;position: relative;float: none;margin: 0;padding: 0;line-height: normal;color: #545454;word-wrap: break-word;">
                            Procesando petición
                        </div>
                    `,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    allowEnterKey: false,
                    showConfirmButton: false
                });
                // llamada
                const resp = await this.apiService.POST(`rest-auth/password/reset/`,
                    post, 'ro', false, false, false, 'body');
                swal(
                    'Exito',
                    'Hemos enviado la información para restablecer su contraseña al correo electrónico proporcionado.',
                    'success'
                );
                this.formaMostrar = 1;
                this.recuperarForm.reset();
            } catch (err) {
                swal(
                    'Hubo un problema',
                    'Error inesperado. Por favor intente de nuevo más tarde.',
                    'warning'
                );
                console.error(err);
            }
        } else {
            // Tocando todos los controles para mostrar los errores
            const controls = this.loginForm.controls;
            for (const control in controls) {
                if (!controls[control].valid) {
                    controls[control].markAsTouched();
                }
            }
        }
    }

}
