import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from './../../servicios/api.service';

@Component({
    selector: 'caffenio-403',
    templateUrl: './403.component.html',
    styleUrls: ['./403.component.css']
})
export class CuatroCeroTresComponent implements OnInit {

    constructor (private router: Router, private apiService: ApiService) {}

    ngOnInit() {

    }

    /** Función para cerrar sesión */
    cerrarsesion() {
        this.apiService.borrarToken();
        this.router.navigate(['/']);
    }

}
