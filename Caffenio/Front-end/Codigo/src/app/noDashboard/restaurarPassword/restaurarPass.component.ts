import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import swal from 'sweetalert2';

import { ApiService } from './../../servicios/api.service';
import { ValidatorsService } from './../../servicios/validators.service';

@Component({
    selector: 'caffenio-restaurar',
    templateUrl: './restaurarPass.component.html',
    styleUrls: ['./restaurarPass.component.css']
})
export class RestaurarPassComponent implements OnInit {

    // Forma
    restaurarForm: FormGroup = new FormGroup({});

    constructor(private fbuilder: FormBuilder, private router: Router, private apiService: ApiService,
        private customValidators: ValidatorsService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            const uid = params['uid'];
            const token = params['token'];
            this.iniciarForma(uid, token);
        });
        // this.iniciarForma(undefined, undefined);
    }

    /**
     * Función para iniciar la forma
     * @param uid
     * @param token
     */
    iniciarForma(uid, token) {
        this.restaurarForm = this.fbuilder.group({
            uid: [(uid) ?  uid : '', []],
            token: [(token) ? token : '', []],
            new_password1: ['', [Validators.required]],
            new_password2: ['', [
                Validators.required,
                (control) => this.customValidators.matchEmail(control, this.restaurarForm.get('new_password1') as FormControl)
            ]]
        });
    }

    async submit(post) {
        const self = this;
        if (this.restaurarForm.valid) {
            // Loading sweetalert
            swal({
                html: `
                    <img src="assets/loading-1.5s.gif" alt="loading">
                    <h2 style="color: #595959;font-size: 30px;text-align: center;font-weight: 600;text-transform: none;position: relative;margin: 0 0 .4em;padding: 0;display: block;word-wrap: break-word;">
                        Espere un momento...
                    </h2>
                    <div style="font-size: 18px;text-align: center;font-weight: 300;position: relative;float: none;margin: 0;padding: 0;line-height: normal;color: #545454;word-wrap: break-word;">
                        Procesando solicitud
                    </div>
                `,
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showConfirmButton: false
            });
            // Llamada al api
            try {
                const resp = await this.apiService.POST(
                    `rest-auth/password/reset/confirm/`,
                    post,
                    'ro',
                    false,
                    false,
                    false,
                    'body'
                );
                swal(
                    'Exito',
                    'Se ha restablecido su contraseña con exito.',
                    'success'
                ).then(() => {
                    this.router.navigate(['/login']);
                });
            } catch (err) {
                console.error(err);
                swal( 'Ocurrio un error.', 'Error inesperado. Por favor intente de nuevo más tarde.', 'error' );
            }
        } else {
            const controls = this.restaurarForm.controls;
            for (const control in controls) {
                if (!controls[control].valid) {
                    controls[control].markAsTouched();
                }
            }
        }
    }

}
