import { Routes } from '@angular/router';

/** Componentes */
import { LoginComponent } from './login/login.component';
import { FormaRegistroComponent } from './registro/formaRegistro.component';
import { RestaurarPassComponent } from './restaurarPassword/restaurarPass.component';

export const noDashboardRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'registro', component: FormaRegistroComponent },
    { path: 'reset', component: RestaurarPassComponent },
    { path: 'reset/:uid/:token', component: RestaurarPassComponent },
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: '**', redirectTo: 'login' }
];
