import { Component } from '@angular/core';

@Component({
    selector: 'caffenio-nodashboard',
    templateUrl: './noDashboard.component.html',
    styleUrls: ['./noDashboard.component.css']
})
export class NoDashboardComponent {

}
