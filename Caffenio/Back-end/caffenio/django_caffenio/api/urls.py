from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework import routers
from .views import *

urlpatterns = {
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
    # users
    url(r'^get-token/', obtain_auth_token),
    url(r'^caffeniousers/$', ProfileView.as_view(), name='users_list'),
    url(r'^caffeniousers/(?P<pk>[0-9]+)/$', ProfileView.as_view(), name='filtered_users_list'),
    url(r'^caffeniousers/current/$', CurrentUserView.as_view(), name='yo_mero'),
    url(r'^caffeniousers/register/$', CreateProfileView.as_view(), name='create_user'),
    # file upload
    url(r'^complementos/$', ComplementoView.as_view(), name='complementos'),
    # auth
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    # correos
    url(r'^invitaciones/$', EnviarInvitacionesView.as_view(), name='invitaciones'),
    # notificaciones
    url(r'^notificaciones/$', NotificacionesCreateView.as_view(), name='notificacion_create'),
    url(r'^notificaciones/(?P<pk>[0-9]+)/$', NotificacionesDetailsView.as_view(), name='notificacion_details'),
    url(r'^notificaciones/company/(?P<company>\w{0,50})/$', NotificacionesCompanyView.as_view(), name='notificacion_company'),
}

urlpatterns = format_suffix_patterns(urlpatterns)