from .models import *
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib import admin
from django.contrib.auth.models import User
from .models import *
#Se define PersonaInline para agregar al create user
class PersonaInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'personas'

#Define un nuevo User Admin
class UserAdmin(BaseUserAdmin):
    inlines = (PersonaInline, )
class ModeloAdmin(admin.ModelAdmin):
    list_display = ('descripcion', 'marca')
class PersonaAdmin(admin.ModelAdmin):
    list_display = ('id', 'user')
class VehiculoAdmin(admin.ModelAdmin):
    list_display = ('numero_economico', 'modelo', 'year')
class FileVehiculoAdmin(admin.ModelAdmin):
    list_display = ('id', 'vehiculo', 'file', 'descripcion')
#Register your models here.

#Se comentó Persona para que el usuario se cree directamente desde UserAdmin (django) directamente.
#admin.site.register(Profile, PersonaAdmin)

#Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)