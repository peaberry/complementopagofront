from rest_framework import generics, status, permissions
from rest_framework.authentication import BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from django.core.mail import send_mail, send_mass_mail
from .serializers import UserSerializer, ProfileSerializer, ComplementoSerializer, NotificacionSerializer
from django.contrib.auth.models import User
from .permissions import IsOwner
from .models import *

# Create your views here.

# User stuff
class ProfileView(APIView):
    """A class based view for fetching user records."""
    authentication_classes = (BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk=None, format=None):
        """
        Get all the user records
        :param format: Format of the users records to return to
        :return: Returns a list of users records
        """
        if pk:
            users = Profile.objects.filter(id=pk)
            serializer = ProfileSerializer(users, many=True)
        else:
            users = Profile.objects.all()
            serializer = ProfileSerializer(users, many=True)
        return Response(serializer.data)

class CreateProfileView(APIView):
    """A class based view for creating users with a Profile info"""
    permission_classes = (AllowAny,)

    def post(self, request):
        """"""
        """serializer = ProfileSerializer(data=request.data)
        if serializer.is_valid(raise_exception=ValueError):
            serializer.create(validated_data=request.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.error_messages, status=status.HTTP_400_BAD_REQUEST)"""
        data = request.data
        user_data = data.pop('user')
        store_user = User.objects.get(username=user_data['username'], email=user_data['email'])
        if store_user.is_active is False:
            store_user.is_active = True
            store_user.set_password(user_data['password'])
            store_user.save()
            return Response("El usuario esta activo", status=status.HTTP_201_CREATED)
        else:
            return Response("Ha ocurrido un error intente más tarde, o contacte al administrador", status=status.HTTP_400_BAD_REQUEST)





class CurrentUserView(APIView):
    """Return the user own Profile model"""
    authentication_classes = (BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    """ def get(self, request):
        user = Profile.objects.filter(user__username = request.user)
        if user:
            serializer = ProfileSerializer(user[0])
            return Response(serializer.data)
        else:
            return Response([], status=status.HTTP_404_NOT_FOUND) """

    def get(self, request):
        profile = Profile.objects.filter(user__username=request.user)
        if profile:
            serializer = ProfileSerializer(profile[0])
            return Response(serializer.data)
        else:
            user = User.objects.filter(username=request.user)
            if user:
                serializer = ProfileSerializer({
                    'user': user[0],
                    'providerId': '',
                    'providerName': 'Administrador: ' + user[0].username,
                    'rfc': '',
                    'companyId': user[0].companyId,
                })
                return Response(serializer.data)
            else:
                return Response([], status=status.HTTP_404_NOT_FOUND)


# Complementos
class ComplementoView(APIView):
    """View complementos"""
    authentication_classes = (BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser)

    def get(self, request, format=None):
        """Get complementos"""
        complementos = Complemento.objects.all()
        serializer = ComplementoSerializer(complementos, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        complemento_serializer = ComplementoSerializer(data=request.data)
        if complemento_serializer.is_valid():
            complemento_serializer.save(
                owner=self.request.user
            )
            # Enviar correos aqui ?
            correos = NotificacionSub.objects.filter(company = request.data['company'])
            emails = []
            for email in correos:
                emails.append(email.email)
            send_mail('subject', 'message', 'contacto@caffenio.com', emails)
            # Respuesta a la llamada
            return Response(complemento_serializer.data,
            status=status.HTTP_201_CREATED)
        else:
            return Response(complemento_serializer.errors,
            status=status.HTTP_400_BAD_REQUEST)


# Emailing
class EnviarInvitacionesView(APIView):
    """
    View para enviar invitaciones a correos segun el RFC
    request.data = {
        "emails": [
            {
                "ProviderId": "PRV0000001",
                "ProviderName": "provedor uno",
                "RFC": "CPA030421M70",
                "Email": "provider@host.com"
            },
            {...},
        ]
    }
    """
    authentication_classes = (BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsAdminUser,)

    def post(self, request):
        emails = []
        for email in request.data['emails']:
            emails.append(email['email'])
            serializer = ProfileSerializer(data=email)
            if serializer.is_valid(raise_exception=ValueError):
                serializer.create(validated_data=email)

        resp = send_mail('subject', 'message', 'contacto@caffenio.com', emails)
        if (resp):
            return Response('success', status=status.HTTP_200_OK)
        else:
            return Response('error', status=status.HTTP_400_BAD_REQUEST)


# Notificaciones
class NotificacionesCreateView(generics.ListCreateAPIView):
    """View crud de notificaciones"""
    authentication_classes = (BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsAdminUser,)

    queryset = NotificacionSub.objects.all()
    serializer_class = NotificacionSerializer
    permission_classes = (permissions.IsAuthenticated,)

class NotificacionesDetailsView(generics.RetrieveUpdateDestroyAPIView):
    """This class handles get, put, delete requests."""
    authentication_classes = (BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsAdminUser,)

    queryset = NotificacionSub.objects.all()
    serializer_class = NotificacionSerializer
    permission_classes = (permissions.IsAuthenticated,)

class NotificacionesCompanyView(APIView):
    """Return notificaciones by company"""
    authentication_classes = (BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated, IsAdminUser,)

    def get(self, request, company=None):
        notificaciones = NotificacionSub.objects.filter(company = company)
        print(notificaciones)
        if (notificaciones):
            serializer = NotificacionSerializer(notificaciones, many=True)
            return Response(serializer.data)
        else:
            return Response([], status=status.HTTP_404_NOT_FOUND)