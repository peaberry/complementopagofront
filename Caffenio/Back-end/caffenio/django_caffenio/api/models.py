from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from datetime import datetime
from os.path import splitext, join

def content_file_name(instance, filename):
    # obteniendo providerId
    providerId = Profile.objects.filter(user__username = instance.owner)[0].providerId
    # fecha
    today = datetime.today()
    # nombre
    nombre = str(providerId) + '-' + today.strftime('%Y%m%d') + '-' + str(instance.folio) + splitext(filename)[1]
    # path
    path = join(str(instance.company), str(providerId), str(today.year), str(today.month), str(today.day), nombre)
    return path

# Create your models here.

class Profile(models.Model):
    """This class represents the user profile info"""
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    providerId = models.CharField(max_length=10, blank=False, unique=True, default='')
    providerName = models.CharField(max_length=255, blank=False, default='')
    rfc = models.CharField(max_length=13, blank=False, default='')
    companyId = models.CharField(max_length=10, blank=False, default='')

@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **Kwargs):
    if created:
        Token.objects.create(user=instance)


"""This class represents a complemento model"""
class Complemento(models.Model):
    xml = models.FileField(upload_to=content_file_name, blank=False, null=False)
    pdf = models.FileField(upload_to=content_file_name, blank=False, null=False)
    company = models.CharField(max_length=255)
    folio = models.CharField(max_length=255)
    desc = models.CharField(max_length=255)
    date_created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey('auth.User', related_name='provider', null=True, on_delete=models.SET_NULL)
    """ owner = models.ForeignKey('auth.User', related_name='provider', null=True, blank=True, on_delete=models.SET_NULL) """


"""This class represents a notification register model"""
class NotificacionSub(models.Model):
    email = models.EmailField(max_length=244, blank=False, default='')
    company = models.CharField(max_length=255, blank=False, default='')

    class Meta:
        unique_together = ('email', 'company',)

