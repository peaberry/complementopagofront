from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.contrib.auth.models import User
from .models import Profile, Complemento, NotificacionSub

"""A user serializer to aid in authentication and authorization."""
class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all())])

    class Meta:
        model = User
        fields = ('username', 'password', 'email', 'is_staff', 'is_active')


class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(required=True)

    class Meta:
        model = Profile
        fields = ('user', 'providerId', 'providerName', 'rfc', 'companyId')

    def create(self, validated_data):
        """"""
        user_data = validated_data.pop('user')
        user = User.objects.create_user(username=user_data['username'], email=user_data['email'], password=user_data['password'], is_active=user_data['is_staff'])
        ''' user.save() '''
        ''' user = UserSerializer.create(UserSerializer(), validated_data=user_data) '''
        profile, created = Profile.objects.update_or_create(
            user=user,
            providerId=validated_data.pop('providerId'),
            providerName=validated_data.pop('providerName'),
            rfc=validated_data.pop('rfc'),
            companyId=validated_data.pop('companyId'),
        )
        return profile


class ComplementoSerializer(serializers.ModelSerializer):
    """Serializer of complemento model."""
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = Complemento
        fields = ('xml', 'pdf', 'company', 'folio', 'desc', 'date_created', 'owner')
        read_only_field = ('date_created')


class NotificacionSerializer(serializers.ModelSerializer):
    """Serializer of notificacion model."""

    class Meta:
        model = NotificacionSub
        fields = ('id', 'email', 'company')